import { Component, OnInit } from '@angular/core';
import { PeriodicTableService } from './periodic-table.service';
import {Table, Row, Element} from './periodic-table.models';


@Component({
  selector: 'app-periodic-table',
  templateUrl: './periodic-table.component.html',
  styleUrls: ['./periodic-table.component.scss']
})
export class PeriodicTableComponent implements OnInit {

  table: Table = [];
  highlighted: string;
  flipped: number = 0;

  constructor(private periodicTableService: PeriodicTableService) { }

  ngOnInit(): void {
    this.mapTable();
  }

  heighlight(block: string) {
    this.highlighted = block;
  }

  flip() {
    this.flipped++;
    this.table.reverse();
    if (this.flipped==5) {window.open('https://www.youtube.com/watch?v=zGM-wSKFBpo');}
    // Yes. Easter Eggs are silly. Epecially if the idea is that people should read the code.
  }

  private mapTable() {
    const elements = this.periodicTableService.getTable();
    elements.forEach(e => {
      if (typeof this.table[e.row-1] === 'undefined') {
        this.table[e.row-1] = [];
      }
      this.table[e.row-1][e.column-1] = e;
      // Minus one to line it up as arrays.
    });
  }

}
