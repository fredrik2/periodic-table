import { Injectable } from '@angular/core';
import table from './periodic-table.constants';

@Injectable({
  providedIn: 'root'
})
export class PeriodicTableService {

  constructor() { }

  getTable() {
    return table;
  }
}
